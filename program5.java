import java.util.Arrays;
class Solution {
    public int majorityElement(int[] nums) {

        if(nums.length==1){
            return nums[0];
        }
        Arrays.sort(nums);
        int count=1;
        for(int i=0;i<nums.length-1;i++){
            
            if(nums[i]==nums[i+1]){
                count++;
            }
             
             else {
                count =1;
            }
            if (count>nums.length/2){
                return nums[i];
            }
        }
        return -1;
    }
    public static void main(String[]args){
		int arr[]={3,2,3};
	    Solution s= new Solution();
	    System.out.println(s.majorityElement(arr));
    }
}


