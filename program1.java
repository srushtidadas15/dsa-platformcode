import java.util.HashSet;

class Solutin {

    int firstRepeatingElement(int[] nums) {

        HashSet set = new HashSet();
        int number = 0;

        for (int i = 0; i < nums.length; i++) {
            if (set.contains(nums[i])) {
                number = nums[i];
                break;

            } else {
                set.add(nums[i]);
            }
        }
        for (int i = 0; i < nums.length; i++) {

            if (nums[i] == number) {
                return i;
            }
        }
        return -1;

    }

    public static void main(String[] args) {

        int[] nums = { 4, 2, 5, 2, 4 };

        Solutin obj = new Solutin();
        int val = obj.firstRepeatingElement(nums);

        System.out.println(val);
    }
}
