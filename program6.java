class CountPair{
	int countPair(int arr[],int k){
			int count=0;
		for(int i=0;i<arr.length-1;i++){
			
			for(int j=i+1;j<arr.length;j++){

				if(arr[i]+arr[j]==k && i!=j){

					count++;
				} 
			}  
		} return count;
		
	}
	public static void main(String[]args){
		int arr[]={1,5,7,1};
		int k= 6;
		CountPair obj=new CountPair();
		System.out.println(obj.countPair(arr,k));
	}
}
